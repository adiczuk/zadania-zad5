#!/usr/bin/env python
# -*- coding=utf-8 -*-

import datetime


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""


def application(environ, start_response):
    response_body = body % (
         environ.get('SERVER_NAME', 'Brak nazwy serwera'), # nazwa serwera
         environ.get('SERVER_ADDR', 'Brak IP serwera'),#environ['SERVER_ADDR'], # IP serwera
         environ.get('SERVER_PORT', 'Brak port serwera'),#environ['SERVER_PORT'], # port serwera
         environ.get('REMOTE_USER', 'Brak nazwa klienta'),#environ['REMOTE_USER'], # nazwa klienta
         environ.get('REMOTE_ADDR', 'Brak IP klienta'),#environ['REMOTE_ADDR'], # IP klienta
         environ.get('REMOTE_PORT', 'Brak port klienta'),#environ['REMOTE_PORT'], # port klienta
         environ.get('SCRIPT_NAME', 'Brak nazwa skryptu'),#environ['SCRIPT_NAME'], # nazwa skryptu
         datetime.datetime.now(),#environ.get('DATE_GMT', 'Brak bieżący czas'),#environ['DATE_GMT'], # bieżący czas
         datetime.datetime.now(),#environ.get('DATE_GMT', 'Brak czas ostatniej wizyty'),#environ['DATE_GMT'], # czas ostatniej wizyty
    )
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 19991, application)
    srv.serve_forever()