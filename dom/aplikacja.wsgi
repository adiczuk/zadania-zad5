﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

from bookdb import BookDB

def application(environ, start_response):
    ksiazki = BookDB()
    response_body = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body>"
    tytuly_ksiazek = ksiazki.titles()

    path_info = environ['PATH_INFO']

    if(path_info == '/'):
        path_info = '/~p7/wsgi'

    if(path_info == '/~p7/wsgi/' or path_info == '/~p7/wsgi'):
        response_body += "<ul>"
        for ksiazka in tytuly_ksiazek:
            response_body += "<li><a href=\'/~p7/wsgi/id_ksiazki="+ksiazka['id']+"\'>" + ksiazka['title'] +"</a></li>"
        response_body += "</ul>"
        status = '200 OK'
    elif("/~p7/wsgi/id_ksiazki=" in path_info):
        id = path_info[21:]
        try:
            info = ksiazki.title_info(id)
            response_body += '<b>Title:</b> ' + info['title'] +"<br/>"
            response_body += '<b>ISBN:</b> ' + info['isbn'] +"<br/>"
            response_body += '<b>Publisher:</b> ' + info['publisher'] +"<br/>"
            response_body += '<b>Author:</b> ' + info['author'] +"<br/>"
            response_body += "<a href=\'/~p7/wsgi/\'>Back</a>"
        except:
            response_body += "<p>Nie ma takiego id w bazie!</p>"
        status = '200 OK'
    else:
        response_body +="<h2>404 NOT FOUND</h2>"
        status = '404 Not Found'

    response_body +="</body></html>"
    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)
    return [response_body]

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 19991, application) #194.29.175.240
    srv.serve_forever()