import unittest
import httplib

class HTTPServerTests(unittest.TestCase):

    main_page = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><ul><li><a href='/~p7/wsgi/id_ksiazki=id4'>Python Cookbook, Second Edition</a></li><li><a href='/~p7/wsgi/id_ksiazki=id5'>The Pragmatic Programmer: From Journeyman to Master</a></li><li><a href='/~p7/wsgi/id_ksiazki=id2'>Python for Software Design: How to Think Like a Computer Scientist</a></li><li><a href='/~p7/wsgi/id_ksiazki=id3'>Foundations of Python Network Programming</a></li><li><a href='/~p7/wsgi/id_ksiazki=id1'>CherryPy Essentials: Rapid Python Web Application Development</a></li></ul></body></html>"
    nginx = '<html>\n<head>\n<title>Welcome to nginx!</title>\n</head>\n<body bgcolor="white" text="black">\n<center><h1>Welcome to nginx!</h1></center>\n</body>\n</html>\n'
    ksiazka1 = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><b>Title:</b> Python Cookbook, Second Edition<br/><b>ISBN:</b> 978-0-596-00797-3<br/><b>Publisher:</b> OReilly Media<br/><b>Author:</b> Alex Martelli, Anna Ravenscroft, David Ascher<br/><a href='/~p7/wsgi/'>Main</a></body></html>"
    ksiazka2 = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><b>Title:</b> The Pragmatic Programmer: From Journeyman to Master<br/><b>ISBN:</b> 978-0201616224<br/><b>Publisher:</b> Addison-Wesley Professional (October 30, 1999)<br/><b>Author:</b> Andrew Hunt, David Thomas<br/><a href='/~p7/wsgi/'>Main</a></body></html>"
    ksiazka3 = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><b>Title:</b> Python for Software Design: How to Think Like a Computer Scientist<br/><b>ISBN:</b> 978-0521725965<br/><b>Publisher:</b> Cambridge University Press; 1 edition (March 16, 2009)<br/><b>Author:</b> Allen B. Downey<br/><a href='/~p7/wsgi/'>Main</a></body></html>"
    ksiazka0 = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><p>Nie ma takiego id w bazie!</p></body></html>"
    not_found = "<html><head><meta charset=\"utf-8\" /><title>PD5: Aplikacja WSGI</title></head><body><h2>404 NOT FOUND</h2></body></html>"

    def zwroc_informacje(self, metoda, sciezka):
        conn = httplib.HTTPConnection('194.29.175.240')
        conn.request(metoda, sciezka)
        return conn.getresponse()

    def sprawdz_poprawnosc(self, wynik, status, reason, header, strona):
        self.assertEqual(status, wynik.status)
        self.assertEqual(reason, wynik.reason)
        czy_jest = False
        for h in wynik.getheaders():
            if header == h[1] and h[0] == 'content-type':
                czy_jest = True
                break
        self.assertEqual(czy_jest, True)
        self.assertEqual(wynik.read(), strona)

    def test_strona_glowna(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.main_page)

    def test_zly_adres(self):
        wynik = self.zwroc_informacje('GET', '/zly')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.nginx)

    def test_ksiazka1(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi/id_ksiazki=id4')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.ksiazka1)

    def test_ksiazka2(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi/id_ksiazki=id5')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.ksiazka2)

    def test_ksiazka3(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi/id_ksiazki=id2')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.ksiazka3)

    def test_ksiazka0(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi/id_ksiazki=id9')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html', self.ksiazka0)

    def test_404(self):
        wynik = self.zwroc_informacje('GET', '/~p7/wsgi/gdsgsdg')
        self.sprawdz_poprawnosc(wynik, 404, 'Not Found', 'text/html', self.not_found)

if __name__ == '__main__':
    unittest.main()